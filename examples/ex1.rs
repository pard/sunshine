use sunshine::player::Player;
use sunshine::framebuffer::Framebuffer;
use sunshine::gamestate::GameState;
use sunshine::map::Map;
use sunshine::renderer::Renderer;
use sunshine::sprite::Sprites;
use sunshine::texture::Texture;

const PI: f64 = std::f64::consts::PI;

fn main() -> std::io::Result<()> {
    let win_w: usize = 1024;
    let win_h: usize = 512;

    let mut framebuffer: Framebuffer = Framebuffer::new(win_h, win_w);

    let map_string = "0000000000000000\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0              0\
                      0000000000000000";
    let map: Map = Map::new(16, 16, map_string);

    let player: Player = Player{
        x: 1.5,
        y: 1.5,
        direction: 1.5,
        fov: PI / 3.,
    };

    let sprites: Sprites = vec![
    ];

    let wall_textures: Texture = Texture::new("./examples/walltex.png");
    let mon_textures: Texture = Texture::new("./examples/montex.png");
    let mut gamestate: GameState = GameState::new(map, player, sprites, mon_textures, wall_textures);
    let mut renderer: Renderer = Renderer::new(20.0);
    let frames = 360;
    for frame in 0..frames {
        renderer.render(&gamestate, &mut framebuffer);
        gamestate.player.direction += 2.*PI/360.;
        framebuffer.write_file(&format!("frame{}.ppm", frame))?;
    }
    Ok(())
}
