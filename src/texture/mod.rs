use image::GenericImageView;
use std::path::Path;
use super::RGB;

pub struct Texture {
    pub image_width: usize,
    pub image_height: usize,
    pub count: usize,
    pub size: usize,
    image: Vec<RGB>,
}

impl Texture {
    pub fn new(filename: &str) -> Texture {
        let image = image::open(Path::new(filename)).unwrap();
        let width = image.width() as usize;
        let height = image.height() as usize;
        let count = width / height;
        let size = width / count;

        if width != height * count {
            panic!("Texture file must contain N square texture packed horizontally.")
        }

        let mut texture: Vec<RGB> = vec![RGB{r:255, g:255, b:255, a:255}; (width * height) as usize];
        for y in 0..height as u32 {
            for x in 0..width as u32 {
                let pix = RGB{
                    r: image.get_pixel(x, y).0[0] as u8,
                    g: image.get_pixel(x, y).0[1] as u8,
                    b: image.get_pixel(x, y).0[2] as u8,
                    a: image.get_pixel(x,y).0[3] as u8,
                };
                let coord: usize = (x + y * width as u32) as usize;
                texture[coord] = pix;
            }
        }
        Texture{
            image_width: width,
            image_height: height,
            count: count,
            size: size,
            image: texture,
        }
    }

    pub fn get(&self, x: usize, y: usize, idx: usize) -> &RGB {
        assert!(x < self.size && y < self.size && idx < self.count);
        &self.image[x + idx * self.size + y * self.image_width]
    }

    pub fn scale_column(&self, texture_id: usize, texture_coord: usize, column_height: usize) -> Vec<RGB> {
        assert!(texture_coord < self.size && texture_id < self.count);
        let mut column: Vec<RGB> = vec![RGB{r:255, g:255, b:255, a:255}; column_height];
        for y in 0..column_height {
            column[y] = *self.get(texture_coord, (y * self.size) / column_height, texture_id);
        }
        column
    }
}
