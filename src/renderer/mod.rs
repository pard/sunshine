use super::{Framebuffer, GameState, PI, Player, RGB, Sprite, Sprites, Texture};

const BLUE: RGB = RGB{r:0,g:50,b:200,a:255};
const DARK_GRAY: RGB = RGB{r:20,g:20,b:20,a:255};

pub struct Renderer {
    focal_distance: f64,
}

impl Renderer {
    pub fn new(focal_distance: f64) -> Renderer {
        Self {focal_distance}
    }

    pub fn render(&mut self, game: &GameState, mut framebuffer: &mut Framebuffer) {
        let mut z_buffer: Vec<f64> = vec![1e3; framebuffer.width];

        // Render walls and cone
        let middle = framebuffer.height as f64 / 2.;
        for i in 0..framebuffer.width {
            let ray_angle: f64 = (1. - i as f64 / framebuffer.width as f64) * (game.player.direction - game.player.fov / 2.) + i as f64 / framebuffer.width as f64 * (game.player.direction + game.player.fov / 2.);
            let x_offset = ray_angle.cos();
            let y_offset = ray_angle.sin();

            let mut distance = 0.0;
            while distance <= self.focal_distance {
                distance += 0.01;

                let ray_x: f64 = game.player.x + distance * x_offset;
                let ray_y: f64 = game.player.y + distance * y_offset;

                let x = cap_f64(ray_x, 15);
                let y = cap_f64(ray_y, 15);

                match game.map.is_empty(x, y) {
                    // Cone, Ceiling, Floor
                    true => {
                        ()
                    },
                    // Walls
                    false => {
                        let projected_distance = distance * (ray_angle - game.player.direction).cos();
                        let wall_height = framebuffer.height as f64 / projected_distance;

                        z_buffer[i] = projected_distance;

                        let texture_id = game.map.get(x, y);
                        let texture_x: isize = wall_x_texture_coord(ray_x, ray_y, &game.wall_textures);
                        let column: Vec<RGB> = game.wall_textures.scale_column(texture_id,
                            texture_x as usize, wall_height as usize);

                        let wall_top = middle - wall_height / 2.;
                        let wall_bottom = wall_top + wall_height;

                        for j in 0..framebuffer.height as isize {
                            if j < wall_top as isize {
                                let mut blue = BLUE.clone();
                                let multiplier = (150. * ( j as f64 / framebuffer.height as f64 * 2.)) as u8;
                                blue.r = multiplier;
                                blue.g += multiplier;
                                framebuffer.set_pixel(i, j as usize, blue);
                            } else if j >= wall_bottom as isize {
                                let mut gray = DARK_GRAY.clone();
                                let multiplier = (50. * ( j as f64 / framebuffer.height as f64 * 2.)) as u8;
                                gray.r += multiplier;
                                gray.g += multiplier;
                                gray.b += multiplier;
                                framebuffer.set_pixel(i, j as usize, gray);
                            } else {
                                let texture_y = (j as f64 - wall_top) / wall_height * column.len() as f64;
                                framebuffer.set_pixel(i, j as usize, column[texture_y as usize]);
                            }
                        }
                        break
                    },
                }
            }
        }

        // Show sprites on map and render to screen
        let sorted_sprites = update_sprites(game.sprites.clone(), &game.player);
        for sprite in sorted_sprites {
            //render_sprite_to_map(&sprite, &mut framebuffer, &map);
            render_sprite_to_screen(&sprite, &game.mon_textures, &z_buffer, &mut framebuffer, &game.player);
        }
    }
}

fn update_sprites(mut sprites: Sprites, player: &Player) -> Sprites {
    for i in 0..sprites.len() {
        sprites[i].player_distance = ((player.x - sprites[i].x).powf(2.)
                                    + (player.y - sprites[i].y).powf(2.)).sqrt();
    }
    sprites.clone().sort_by(|a, b|
        b.player_distance.partial_cmp(&a.player_distance).unwrap());
    sprites
}

fn render_sprite_to_screen(sprite: &Sprite, mon_textures: &Texture, z_buffer: &Vec<f64>,
                           framebuffer: &mut Framebuffer, player: &Player) {

    let mut sprite_direction: f64 = (sprite.y - player.y).atan2(sprite.x - player.x);
    while sprite_direction - player.direction > PI { sprite_direction -= 2.*PI; };
    while sprite_direction - player.direction < -PI { sprite_direction += 2.*PI; };

    let sprite_screen_size: isize = 1000isize.min((framebuffer.height as f64 / sprite.player_distance) as isize);

    let v_offset: isize = (framebuffer.height as f64 / 2. - sprite_screen_size as f64 / 2.) as isize;
    let h_offset: isize = ((sprite_direction - player.direction)
                        / player.fov * (framebuffer.width as f64 / 2.0)
                        + (framebuffer.width as f64 / 2.0) / 2.0
                        - mon_textures.size as f64 / 2.0) as isize;

    for i in 0..sprite_screen_size {
        if h_offset + i < 0 || h_offset + i >= framebuffer.width as isize { continue; }
        if z_buffer[(h_offset + i) as usize] < sprite.player_distance { continue; }
        for j in 0..sprite_screen_size {
            if v_offset + j < 0 || v_offset + j >= framebuffer.height as isize { continue; }

            let tex_x: usize = i as usize * mon_textures.size / sprite_screen_size as usize;
            let tex_y: usize = j  as usize* mon_textures.size / sprite_screen_size as usize;
            let color: RGB = *mon_textures.get(tex_x, tex_y, sprite.texture_id);

            let x: usize = (framebuffer.width as f64 + h_offset as f64 + i as f64) as usize;
            let y: usize = (v_offset + j) as usize;

            if color.a > 128 { framebuffer.set_pixel(x, y, color); }
        }
    }
}


fn cap_f64(n: f64, capped: usize) -> usize {
    match n as usize {
        x if x > capped => capped,
        _ => n as usize,
    }
}

fn wall_x_texture_coord(x: f64, y: f64, textures: &Texture) -> isize {
    let hit_x: f64 = x - (x + 0.5).floor();
    let hit_y: f64 = y - (y + 0.5).floor();

    let mut texture_x: isize = (hit_x * textures.size as f64) as isize;
    if hit_y.abs() > hit_x.abs() {
        texture_x = (hit_y * textures.size as f64) as isize;
    };
    if texture_x < 0 {
        texture_x += textures.size as isize;
    };
    texture_x
}
