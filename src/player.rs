pub struct Player {
    pub x: f64,
    pub y: f64,
    pub direction: f64,
    pub fov: f64,
}
