use super::{Map, Player, Sprites, Texture};

pub struct GameState {
    pub map: Map,
    pub player: Player,
    pub sprites: Sprites,
    pub mon_textures: Texture,
    pub wall_textures: Texture,
}

impl GameState {
    pub fn new(map: Map, player: Player,
              sprites: Sprites, mon_textures: Texture,
              wall_textures: Texture) -> GameState {
        GameState{
            map, player, sprites,
            mon_textures, wall_textures
        }
    }

    pub fn render(&mut self) {
        todo!();
    }
}
