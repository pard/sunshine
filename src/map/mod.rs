pub struct Map {
    pub width: usize,
    pub height: usize,
    pub map: Vec<char>,
}

impl Map {
    pub fn new(width: usize, height: usize, map: &str) -> Map {
        let v_map: Vec<char> = map.chars().collect();
        Map{ width: width, height: height, map: v_map }
    }

    pub fn get(&self, x: usize, y: usize) -> usize {
        assert!(x < self.width && y < self.height && 
                self.map.len() == self.width * self.height);
        self.map[x + y * self.width] as usize - '0' as usize
    }

    pub fn is_empty(&self, x: usize, y: usize) -> bool {
        assert!(x < self.width && y < self.height && 
                self.map.len() == self.width * self.height);
        self.map[x + y * self.width] == ' '
    }

    pub fn get_char(&self, x: usize, y: usize) -> char {
        assert!(x < self.width && y < self.height && 
                self.map.len() == self.width * self.height);
        self.map[x + y * self.width]
    }
}
