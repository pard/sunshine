#[derive(Clone, Copy, PartialEq, PartialOrd)]
pub struct Sprite {
    pub x: f64,
    pub y: f64,
    pub player_distance: f64,
    pub texture_id: usize,
}

impl Sprite {
    pub fn new(x: f64, y: f64, texture_id: usize) -> Sprite {
        Sprite{
            x: x,
            y: y,
            player_distance: 0.,
            texture_id: texture_id,
        }
    }
}

pub type Sprites = Vec<Sprite>;
