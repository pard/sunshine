# Sunshine

A 2.5D raytracing engine built on miniquad, inspired by Love2D

## Why

I am developing Sunshine to explore game design, engine design, and to work on
writing better Rust software. I picked the raytracing/2.5D space because I
found a distinct lack of purpose built engines for it and I think that it is
an game technology that has not been explored as much as it ought to be.

One of the really cool things about the roguelike scene is that the minimalistic
graphics really get out of the way and let developers explore concepts and ideas
that would either be far more difficult to manage and implement in a full 3D
space. Using raytracing we can emulate some of the allure 3D games provide, but
without adding any/much difficulty. In fact because 2.5D raytracing is really
just a 2D game with some rendering tricks to make it look 3D, much of the
roguelike design paradigms transfer over seamlessly.

## Todo
- [ ] raytracing engine
- [ ] voxel space support
- [ ] input support
